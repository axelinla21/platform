<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

define('ALLOW_UNFILTERED_UPLOADS', true);

define('WP_MEMORY_LIMIT', '64M');

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'platform' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '}|3O||<YYs{gW#}Nov6&nKa2`>UWG*!%pXHG0#E.8@#MCDz|28y?I!xgp?x`XTH|' );
define( 'SECURE_AUTH_KEY',  '9W[v|hM!IHoV^4j`KJbEXA1DL/wR|9xcg2a(#-GL0@q)e>z]O#`jU(d>.hfXxu|T' );
define( 'LOGGED_IN_KEY',    '-uT`G%]]#O<FRhL?tB9M1h$;[}mzA$Fl)xNRibg{Ov&fmSeZqx;5M+)t-jjl&AKa' );
define( 'NONCE_KEY',        'AjaY!0u(bwW*wue~EP,Y5*EAI0GZqv2|`*t/b,qJP.a6~PZ`4C8+^.R2idi]v:}F' );
define( 'AUTH_SALT',        'Xb8JTs4F}2@Yw2.pL`3bkl%L/a0tYFAhqngEcbQ!~!]Qt!}$kBP<VzJ%G8d@Wl!;' );
define( 'SECURE_AUTH_SALT', 'ofyf[e9Dv<_=,ld1w;DVO#P]O82DT3`0i-HjQt^$O|g0XXmCLOPZQ.AqQvS<~)8N' );
define( 'LOGGED_IN_SALT',   'A(@-A;f@w%iqdYP4!u|h_Rq6,lG%5i?PTX~Hv,R*tkNLtcZdfZ2>Sa)u9-QDNH;p' );
define( 'NONCE_SALT',       '@r+ $G^G sbMN7<[c}g5,-^G1E}r,P7Hrc*{{/L!A|M?BSSndiih7m&B@w?EMQ<$' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
