<?php

/*****
 * Template Name: invoices
 */

// Start de sessie
session_start();

get_header();

include("getNav.php");

//Controleer bij de sessie of je aangemeld bent
if (!isset($_SESSION['aangemeld'])) {
    header('Location: index');
    exit();
}

// koppel username aan sessie naam
$id = $_SESSION['id'];
$username = $_SESSION['username'];

//// als username leeg is dan laat hij alle mapjes zien, dus exit dan als voorzorg
if ($username == '' || $username == null) {
    exit();
}

?>

<!DOCTYPE html>
<html>
<!--    <div> <?php echo 'welkom ' . $username . '<br>'; ?> </div>-->

     <div id="zoekFunctie" style="padding-bottom:5%">
             <h4> Zoek hier naar je project </h4>
            <input id="inputSearch" type="text" placeholder="Search.."> <i id="searchIcon" onclick="searchProject()" class="fas fa-search"> </i>
        </div>
    
    <?php echo checkInvoices($id); ?>
             
    <?php get_footer(); ?>
    

<script>
 
     let x = document.getElementById('inputSearch');
    
    // op enter klik bij het input field activeer functie
    x.addEventListener("keyup", function(event) {
      if (event.keyCode === 13) {
       event.preventDefault();
       document.getElementById("searchIcon").click();
      }
    });
     
</script> 