<?php
/*****
 * Template Name: logout
 */

// We need to use sessions, so you should always start sessions using the below code.
session_start();
// If the user is not logged in redirect to the login page...
if (!isset($_SESSION['aangemeld'])) {
	header('Location: ');
	exit();
}
?>

<?php
get_header();
?>


<?php
session_start();
session_destroy();
// Redirect to the login page:
header('Location: ../');
?>



<?php
get_footer();  
?>