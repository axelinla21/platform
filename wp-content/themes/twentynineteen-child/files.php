<?php

/*****
 * Template Name: files
 */

// Start de sessie
session_start();

get_header();

include("getNav.php");

//Controleer bij de sessie of je aangemeld bent
if (!isset($_SESSION['aangemeld'])) {
    header('Location: index');
    exit();
}

// koppel username aan sessie naam
$id = $_SESSION['id'];
$username = $_SESSION['username'];

//// als username leeg is dan laat hij alle mapjes zien, dus exit dan als voorzorg
if ($username == '' || $username == null) {
    exit();
}

?>

<!DOCTYPE html>
<html>
<!--    <h2>Profile Page</h2>-->
<!--    <div> <?php echo 'welkom ' . $username . '<br>'; ?> </div>-->
            
<!--    <button id="openDirectories" onclick="openAllDirectories("")"> open all directories </button>-->

    
<!--    zoekbalk -->
        <div id="zoekFunctie">
             <h4> Zoek hier naar je project </h4>
            <input id="inputSearch" type="text" placeholder="Search.."> <i id="searchIcon" onclick="searchProject()" class="fas fa-search"> </i>
        </div>
    
    
    <?php echo listFolderFiles($id); ?>
             
    <?php get_footer(); ?>
    
    <script>
            // enable toggling between directories    
        var toggler = document.getElementsByClassName("caret");
        var i;
        
        for (i = 0; i < toggler.length; i++) {
            // verander de klik on click
            toggler[i].addEventListener("click", function() {
            this.parentElement.querySelector(".nested").classList.toggle("active");
            this.classList.toggle("caret-down"); 
          });
        }   

    
    let x = document.getElementById('inputSearch');
    
    // op enter klik bij het input field activeer functie
    x.addEventListener("keyup", function(event) {
      if (event.keyCode === 13) {
       event.preventDefault();
       document.getElementById("searchIcon").click();
      }
    });
        
        
    </script>