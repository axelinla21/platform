<?php

/*****
 * Template Name: home
*/

// We need to use sessions, so you should always start sessions using the below code.
session_start();

get_header();

include("getNav.php");

// If the user is not logged in redirect to the login page...
if (!isset($_SESSION['aangemeld'])) {
    header('Location: index');
    exit();
}

 $current_user = wp_get_current_user();

//koppel username aan sessie naam
$username = $current_user->user_login;
// als username leeg is dan laat hij alle mapjes zien, dus exit dan als voorzorg
if ($username == '' || $username == null) {
    exit();
}




?>

<!DOCTYPE html>

<html>
    <div id="profilePage"> 
        <h2>Home Page</h2>
        <p>Welcome back, <?= $_SESSION['name'] ?>!</p>
        <div id="containerSubjects">
            <div class="subjectBlock"> <p> <a href="../profile"> <i id="subjectIcon" class="fas fa-user-circle"> </i> open profile </a> </p> </div>
            <div class="subjectBlock"> <p> <a href="../files"> <i id="subjectIcon" class="fas fa-archive"></i> look at my files  </a> </p> </div>
            <div class="subjectBlock"> <p> <a href="../invoices"> <i id="subjectIcon" class="fas fa-receipt"></i> check your invoices  </a> </p> </div>
        </div>
    </div>
            

<?php get_footer(); ?>