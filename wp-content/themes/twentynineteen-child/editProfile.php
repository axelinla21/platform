<?php
/*****
 * Template Name: editProfile
*/

// We need to use sessions, so you should always start sessions using the below code.
session_start();
// If the user is not logged in redirect to the login page...
if (!isset($_SESSION['aangemeld']))
{
    header('Location: index.html');
    exit();
}

?>

<?php get_header();

$argBedrijven = array(
    'post_type' => 'bedrijven'
);

$the_queryBedrijven = new WP_Query($argBedrijven);

//  laat alle posts zien die bij het ingelogde bedrijf passen
while ($the_queryBedrijven->have_posts())
{

    $the_queryBedrijven->the_post();

    $bedrijfsnaam = get_field('bedrijfsnaam');
    $usernaam = get_field('username');
    $password = get_field('password');
    $email = get_field('email');
    $sessionname = $_SESSION['name'];

    if ($usernaam == $sessionname)
    {
?>  

    <section id="primary" class="content-area">
                    <main id="main" class="site-main">
                        <div class="login">
                            <h1>edit profile</h1>
                            <form action="../publishDataEditProfile" method="post">
                                <p style="width: 360px;margin: 0;"> vul hier je username in: </p>
                                <label for="username">  
                                    <i class="fas fa-user"></i>
                                </label>
                                <input type="text" name="username" placeholder="Username" id="username"  value=<?=$usernaam ?> required>
                                <p style="width: 360px;margin: 0;"> vul hier je password in: </p>
                                <label for="password">
                                    <i class="fas fa-lock"></i>
                                </label>    
                                <input type="text" name="password" placeholder="Password" id="password"  value=<?=$password ?> required>
                                <p style="width: 360px;margin: 0;"> vul hier je email in: </p>
                                <label for="email">
                                    <i class="fas fa-lock"></i>
                                </label>    
                                <input type="text" name="email" id="email" value=<?=$email ?> required>
                               <input type="submit" value="submit">
                            </form>
                        </div>
                    </main><!-- #main -->
                </section><!-- #primary -->
<?php
        
    }
}

?>

    

<?php get_footer() ?>
