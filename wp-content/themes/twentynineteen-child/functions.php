<?php

add_action( 'plugins_loaded', 'rad_plugin_load_text_domain' );
function rad_plugin_load_text_domain() {
    load_plugin_textdomain( 'rad-plugin', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );
}


/* Adds child theme */
add_action( 'wp_enqueue_scripts', 'enqueue_parent_styles' );
function enqueue_parent_styles() {
   wp_enqueue_style( 'parent-style', get_template_directory_uri().'/style.css' );
}

/* Custom Post Type Start */

function create_posttype() {
register_post_type( 'projecten',
// CPT Options

array(
  'labels' => array(
   'name' => __( 'projecten' ),
   'singular_name' => __( 'projecten' )
  ),
  'public' => true,
  'has_archive' => false,
  'rewrite' => array('slug' => 'projecten'),
 )
);
    
    
register_post_type( 'bedrijven',
// CPT Options

array(
  'labels' => array(
   'name' => __( 'bedrijven' ),
   'singular_name' => __( 'bedrijven' )
  ),
  'public' => true,
  'has_archive' => false,
  'rewrite' => array('slug' => 'bedrijven'),
 )
);
    
    
}
// Hooking up our function to theme setup
add_action( 'init', 'create_posttype' );
/* Custom Post Type End */


/* hash password for custom posts */
function ns_function_encrypt_passwords( $value, $post_id, $field )
{
    $value = password_hash($value, PASSWORD_DEFAULT);
 
    return $value;
}
add_filter('acf/update_value/type=password', 'ns_function_encrypt_passwords', 10, 3);


//function to display files
// functie op to controleren of zip/rar
// functie kijkt werkelijk naar de file niet aleen de naam zelf
function isRarOrZip($file) {
    $fh = fopen($file, "r");
    // kijkt naar de lengte, als de lengte niet match met de lengte die een zip/rar heeft dan krijg je false
    $blob = fgets($fh, 5);

    // if file is RAR
    if (strpos($blob, 'Rar') !== false) {
      return true ;
    } else
    // if file is ZIP
    if (strpos($blob, 'PK') !== false) {
      return true;
    } else {
      return false;
    }
}

// functie om te controleren of andere soorten types
// functie kijkt alleen of de string een string-onderdeel en is daardoor minder betrouwbaar
function checkFileType($file){
    if (strpos($file, 'jpg') || strpos($file, 'jpeg') || strpos($file, 'png') !== false) return 'image';  
    if (strpos($file, 'pdf') !== false) return 'pdf';
    else return FALSE;
}

// functie om de files weer te geven die matchen bij de ingelogde user
function listFolderFiles($user){
    
    // check gegevens van user
    $user_info = get_userdata($user);
    $bedrijf = $user_info->bedrijf;
    
    $args = array(
        'post_type' => 'projecten'
    );

    // Query the posts:
    $projecten_query = new WP_Query($args);
    
    $x = 0;
    
    // Loop through the obituaries:
    while ($projecten_query->have_posts()):
    // check which project it is
    
        $projecten_query->the_post();                                      
       
        // kijk naar het gekoppelde bedrjif
        $name = get_field('name'); 
    
        if( $name ) {
            // bepaald wat voor div het wordt 
            $x++;
           
            // geef projecten weer mbt opgegegven naam
            foreach( $name as $p ){
                $naam = get_the_title( $p->ID );  
                
                // gekoppelde usernaam is gelijk aan bedrijfnaam van een project
                if ($naam == $bedrijf){

                     // check wat voor div het is
                        if($x % 4 == 0){
                             echo '<div id="orange" class="container">';
                        } else if($x % 2 == 0){
                           echo '<div id="grey" class="container">'; 
                        } else{
                            echo '<div id="white" class="container">';
                        }
                    
                        ?>
                        <div id="orderNummer"> <p> <span id="ordernummer"> <?php the_field('ordernummers') .'</span>';
                        echo '<span id="titleProject">' .get_the_title() . '</span> </p>';
                     
                    $titel = get_the_title();
                    
                    // create zip file
                    $temp_unzip_path = './wp-content/uploads/2019/10/';
                    $zip = new ZipArchive();
                    $dirArray = array();
                    $new_zip_file = $temp_unzip_path.$titel.'.zip';
                    $new = $zip->open($new_zip_file, ZIPARCHIVE::CREATE);
                    
                    // als de user een admin is
                    if ( in_array( 'administrator', (array) $user_info->roles ) ) {
                         // bekijk de facturen
                         $factuur = get_field('factuur');
                        if($factuur != null) echo '<div id="factuur"> <p> factuur </p> <a href='.$factuur.' download> <button id="factuur" class="download"> download</button> </div> </a> </br>'; 
                    }
                 
                    // geef data van het project weer
                    // check if the repeater field has rows of data
                    if( have_rows('bestanden') ){
                         
                         // loop through the rows of data
                         while ( have_rows('bestanden') ) : the_row();
                           
                             // creeer array van bestand
                             $value = get_sub_field('bestand');
                        
                             // specifieke value toewijzen
                             $urlVal = $value['url'];
                             $idVal = $value['id'];
                            
                             // opschonen om naam te laten zien
                             $neVal = str_replace("http://localhost/wordpress/wp-content/uploads/2019/10/","",$urlVal);
                        
                            if($urlVal != null){
                                // vul de zip file
                                if ($new === true) {
                                    $zip->addFromString($neVal, 'file content goes here');   
                                    
                                } else {
                                        echo 'Failed to create Zip';
                                }
                                         
                                if (checkFileType($urlVal) == 'image'){
                                    echo '<li>'.$neVal.' <a href='.$urlVal.' download> <button class="download"> download</button> </a> <button onclick="preview('."'".$idVal."'".')" class="view"> view </button> <img id='.$idVal.' src='.$urlVal.' style="display:none"> </li>';
                                }  else {
                                    echo '<li>'.$neVal.' <a href='.$urlVal.' download> <button class="download"> download</button> </a> </li>';
                                }    
                            }
                        
                        endwhile;
                                
                        echo '<li> '.$titel.' zip <a href='.$new_zip_file.' download> <button id="zip" class="download"> download zip </button> </a> </li>';
                        
                    } 
                    
                     //print zip en sluit hem
                     $zip->close();
                    
                    echo '</div>';
                } 
                
                 ?> </div> <?php
            }
            
        } else{
           
        }
    
    endwhile;  
}

//creëer nieuwe rol
function wporg_simple_role() {
    add_role(
        'bedrijfsAdmin',
        'bedrijfsAdmin',
        [
            'read'         => true,
            'edit_posts'   => false,
            'upload_files' => false,
        ]
    );
    user_can( $user, $capability );
}

// Add the simple_role.
add_action('init', 'wporg_simple_role');


// redirect gebruikers
function acme_login_redirect( $redirect_to, $request, $user) {
        session_start();
        session_regenerate_id();
        $_SESSION['aangemeld'] = TRUE;
        $_SESSION['id'] = $user->ID;
        $user_info = get_userdata($user->ID);
        $_SESSION['username'] = $user_info->user_login;
    
        return ( is_array( $user->roles ) && in_array( 'administrator', $user->roles ) ) ? admin_url() : site_url('home');
}

add_filter( 'login_redirect', 'acme_login_redirect', 10, 3 );

//voeg extra value 'bedrijf' aan users toe
add_action( 'show_user_profile', 'extra_user_profile_fields' );
add_action( 'edit_user_profile', 'extra_user_profile_fields' );

function extra_user_profile_fields( $user ) { ?>
<h3><?php _e("Extra profile information", "blank"); ?></h3>


<table class="form-table">
<tr>
<th><label for="bedrijf"><?php _e("bedrijf"); ?></label></th>
<td>
<input type="text" name="bedrijf" id="bedrijf" value="<?php echo esc_attr( get_the_author_meta( 'bedrijf', $user->ID ) ); ?>" class="regular-text" /><br />
<span class="description"><?php _e("Please enter your bedrijf. (wordt nog uitgebreid naar selector)"); ?></span>
</td>
</tr>
</table>
<?php }

add_action( 'personal_options_update', 'save_extra_user_profile_fields' );
add_action( 'edit_user_profile_update', 'save_extra_user_profile_fields' );

function save_extra_user_profile_fields( $user_id ) {

if ( !current_user_can( 'edit_user', $user_id ) ) { return false; }
    
    update_user_meta( $user_id, 'bedrijf', $_POST['bedrijf'] );
}

function checkInvoices($user){
    
    // check gegevens van user
    $user_info = get_userdata($user);
    $bedrijf = $user_info->bedrijf;
    
    $args = array(
        'post_type' => 'projecten'
    );

    // Query the posts:
    $projecten_query = new WP_Query($args);
    
    $x = 0;
    
    // Loop through the obituaries:
    while ($projecten_query->have_posts()):
    // check which project it is
    
        $projecten_query->the_post();                                      
       
        // kijk naar het gekoppelde bedrjif
        $name = get_field('name'); 
    
        if( $name ) {
            // bepaald wat voor div het wordt 
            $x++;
           
            // geef projecten weer mbt opgegegven naam
            foreach( $name as $p ){
                $naam = get_the_title( $p->ID );  
                
                // gekoppelde usernaam is gelijk aan bedrijfnaam van een project
                if ($naam == $bedrijf){
                    // als de user een admin is
                    if ( in_array( 'administrator', (array) $user_info->roles ) ) {
                         // bekijk de facturen
                         $factuur = get_field('factuur');
                        if($factuur != null){
                             
                            echo '<div id="containerFactuur" class="container">
                           <a href='.$factuur.' download> <button id="factuur" class="download" style="margin-top: 3%"> download</button> </a>
                                    <div id="facturNummer"> 
                                        <p id="factuurOrders"> 
                                            <span id="ordernummer">' . get_field('ordernummers') .'</span>
                                            <span id="titleProject"> factuur ' .get_the_title() . '</span> </div> 
                                        </p> 
                                    </div>
                                </div>';
                            
                                
                        }
                    }
                }  
            } 
        } 
    
    endwhile;  
}


                            
?>                        