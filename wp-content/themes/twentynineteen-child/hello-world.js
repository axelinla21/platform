const http = require('http');

const hostname = '127.0.0.1';
const port = 3000;

//requiring path and fs modules
const path = require('path');
const fs = require('fs');
//joining path of directory 
const directoryPath = path.join(__dirname, 'backup/files/');


const server = http.createServer((req, res) => {
  res.writeHead(200 , {'Content-Type': 'text/plain'});   
    
});                               
                                
//passsing directoryPath and callback function
fs.readdir(directoryPath, function (err, files) {
//handling error
if (err) {
    return console.log('Unable to scan directory: ' + err);
} 
    
//listing all files using forEach
files.forEach(function (file) {
// Do whatever you want to do with the file
    console.log(file);
     });
});    
    
                          
server.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});

