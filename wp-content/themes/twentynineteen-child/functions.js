
// open en sluit de data   
function preview(id){
     var display = document.getElementById(id).style.display;
     swal({
          title: "Are you sure?",
          text: "Are you sure you want to edit the view to this file?",
          buttons: [
            'No, cancel it!',
            'Yes, I am sure!'
          ],
          dangerMode: true,
        }).then(function(isConfirm) {
          if (isConfirm) {
                // geef data weer of sluit hem 
                 if(display == 'none'){
                    document.getElementById(id).style = 'display:auto';
                } else{
                   document.getElementById(id).style = 'display:none';
                }
          } else {
            swal("Cancelled", "You cancelled this request", "error");
          }
        });
}

// functie op alle directories te openen/sluiten
function openAllDirectories(){
    var toggler = document.getElementsByClassName("caret");
    var toggled = document.getElementsByClassName("caret-down");
    var i;
    
        for (i = 0; i < toggler.length; i++) {
            // sluit alle directories als 1 folder openstaat
            if(toggler[i].classList.value == 'caret caret-down'){
                    
                for (i = 0; i < toggler.length; i++) {
                    toggler[i].classList.value = 'caret';
                    toggler[i].parentElement.querySelector(".nested").classList.value = "nested";  
                }
                
                break;
                     
            // als niks openstaat dan open alle directories
            } else{
                toggler[i].classList.value = 'caret caret-down';
                toggler[i].parentElement.querySelector(".nested").classList.value = "nested active";
            }
 
        }
}

  function searchProject(){
      console.log('activated');
        let input = document.getElementById('inputSearch').value; 
      
        input=input.toLowerCase(); 
        let x = document.getElementById('titleProject').innerText;
        let y = document.getElementsByClassName('container'); 

        for (i = 0; i < y.length; i++) {  
            
            if (!y[i].innerHTML.toLowerCase().includes(input)) { 
                y[i].style.display="none"; 
            } 
            else { 
                y[i].style.display="list-item";                  
            } 
        } 
      
    }

