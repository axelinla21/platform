<?php

/*****
 * Template Name: files
 */

// Start de sessie
session_start();

get_header();

include("getNav.php");

//Controleer bij de sessie of je aangemeld bent
if (!isset($_SESSION['aangemeld'])) {
    header('Location: index');
    exit();
}

// koppel username aan sessie naam
$id = $_SESSION['id'];
$username = $_SESSION['username'];

//// als username leeg is dan laat hij alle mapjes zien, dus exit dan als voorzorg
if ($username == '' || $username == null) {
    exit();
}

 $current_user = wp_get_current_user();
 $username = $current_user->user_login;
 $userPas = $current_user->user_pass;
 $userMail = $current_user->user_email;

?>

    <section id="primary" class="content-area">
                    <main id="main" class="site-main">
                        <div class="login">
                            <h1>edit profile</h1>
                            <form action="../publishDataEditProfile" method="post">
                                <p style="width: 360px;margin: 0;"> vul hier je username in: </p>
                                <label for="username">  
                                    <i class="fas fa-user"></i>
                                </label>
                                <input type="text" name="username" placeholder="Username" id="username"  value=<?=$username ?> required>
                                <p style="width: 360px;margin: 0;"> vul hier je password in: </p>
                                <label for="password">
                                    <i class="fas fa-lock"></i>
                                </label>    
                                <input type="text" name="password" placeholder="Password" id="password"  value=<?=$userPas ?> required>
                                <p style="width: 360px;margin: 0;"> vul hier je email in: </p>
                                <label for="email">
                                    <i class="fas fa-lock"></i>
                                </label>    
                                <input type="text" name="email" id="email" value=<?=$userMail ?> required>
                               <input type="submit" value="submit">
                            </form>
                        </div>
                    </main><!-- #main -->
                </section><!-- #primary -->


    

<?php get_footer() ?>
