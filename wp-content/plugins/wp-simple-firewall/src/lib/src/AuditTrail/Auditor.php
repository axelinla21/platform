<?php

namespace FernleafSystems\Wordpress\Plugin\Shield\AuditTrail;

use FernleafSystems\Wordpress\Plugin\Shield\Databases\AuditTrail\EntryVO;
use FernleafSystems\Wordpress\Services\Services;

/**
 * Trait Auditor
 * @package FernleafSystems\Wordpress\Plugin\Shield\AuditTrail
 * @deprecated 8.1
 */
trait Auditor {
}