<?php

namespace FernleafSystems\Wordpress\Services\Utilities\File;

use FernleafSystems\Wordpress\Services\Services;

/**
 * Class ExtractLineFromFile
 * @package FernleafSystems\Wordpress\Services\Utilities\File
 */
class ExtractLineFromFile {

	/**
	 * @param string $sPath
	 * @param int    $nLine
	 * @return string
	 * @throws \Exception
	 */
	public function run( $sPath, $nLine ) {

		$aLines = $this->getFileAsLines( $sPath );
		if ( !array_key_exists( $nLine, $aLines ) ) {
			throw new \Exception( 'Line does not exist.' );
		}

		return $aLines[ $nLine ];
	}

	/**
	 * @param string $sPath
	 * @return string[]
	 * @throws \Exception
	 */
	protected function getFileAsLines( $sPath ) {
		$oFs = Services::WpFs();
		if ( !$oFs->isFile( $sPath ) ) {
			throw new \InvalidArgumentException( 'File does not exist' );
		}

		$sContents = $oFs->getFileContent( $sPath );
		if ( empty( $sContents ) ) {
			throw new \Exception( 'File is empty' );
		}

		return explode( "\n", $sContents );
	}
}