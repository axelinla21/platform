=== Shield Security: Protection with Smarter Automation ===
Contributors: onedollarplugin, paultgoodchild
Donate link: https://icwp.io/bw
License: GPLv3
License URI: http://www.gnu.org/licenses/gpl.html
Tags: scan, firewall, two factor authentication, login protection, malware
Requires at least: 3.5.2
Requires PHP: 5.4.0
Recommended PHP: 7.0
Tested up to: 5.3
Stable tag: 8.2.2

Security protection from hackers through smarter automation. Powerful scanners, 2-Factor Auth, limit logins, auto IP blocks & more.

== Description ==

### The highest rated 5* Security Plugin for WordPress

Shield - highest average 5* rating for any WordPress security plugin (2019/05). [See what people are saying here](https://wordpress.org/support/plugin/wp-simple-firewall/reviews/?filter=5).

#### It's 2019 - Don't settle for just another security plugin. Get *smarter* security.

You don't need another 100 email notifications.

You **need** a security plugin that does **all** the heavy lifting for you, and only alerts you when you need to know.

And when you get an alert, you actually have steps to take, not just the position of "I have no clue what to do!"

### Security for WordPress *doesn't* have to be *hard*

There's no reason for security to be so darn complicated. It doesn't have to be this way any longer.

Shield is the easiest security plugin to setup - you simply activate it.

And you can gradually dig deeper, as you're ready.

#### Trust: Shield Does Exactly What It Says It Will Do

You've probably been let down in the past, but Shield is the WordPress Security solution that does what it says it'll do - Protect Your Site.

#### Constant notifications are not okay. You're already busy!

Receiving constant alerts from your security plugins isn't "security". It's just noise. By the time you receive a notification and respond to it, it's already too late.

Instead, Shield Security does it what it needs to do, and alerts you if and when you need to informed.

Shield is your Silent Guardian. It doesn't squawk at you every time a visitor presses against your defenses.

It'll do *its job* without moaning at you, and leave you in peace to get on with *your job*.

#### You're not alone, and there's no risk to test it out.

You can try out Shield alongside any other security plugins, and it [comes highly recommended](https://wordpress.org/support/plugin/wp-simple-firewall/reviews/)
- it has the highest average rating for any WP Security plugin.

Easy-to-setup, but powerful protection blocks attacks and suspicious activity, but won't lock you out.

Shield is the must-have, free Security Solution for all your WordPress sites.

= Shield Features You'll Absolutely Love =

* Beautiful, Easy-To-Use Guided Wizards - help you configure Shield and run scans like a Pro
* Limit Login Attempts / Block Automatic Brute-Force Bots - all automatically
* Powerful Core File Scanners - automatically detects malicious file changes and hacks you'd never see
* Automatic IP Black List - no need for you to manage IPs!
* 2-Factor Authentication - including Google Authenticator and Email
* Block 100% Automated Comments SPAM
* Audit Trail & User Activity Logging
* reCAPTCHA
* Firewall
* Security Admin Users
* Block REST API / XML-RPC
* HTTP Headers
* Automatic Updates Control
* and much, much more...

> <strong>Don't Leave Your Site At Risk</strong><br />
> If your site is vulnerable to attack, you're putting your business and your reputation at serious risk. Getting hacked can mean you're locked out of your site, client data stolen, your website defaced or offline, and Google *will* penalise you.
>
> Why take the risk?
>
> Download and install Shield now for FREE so that you have the most powerful WordPress security system working for you and protecting your site.

= The New Shield Pro =

From November 2017, Shield Security now has a Pro version for those that need to take their Security to the next level.

> <strong>The Pro Extras</strong>:
>
> 1. Exclusive customer email support.
> 1. Plugin Vulnerability Scanner.
> 1. Plugin / Themes Hack Detection Scanner.
> 1. More Frequent Scans - as often as every hour.
> 1. Protection for your WooCommerce customers (incl. Easy Digital Downloads & BuddyPress)
> 1. Remember-Me 2-Factor Authentication.
> 1. Powerful Password Policies.
> 1. Import/Export of options across sites.
> 1. Improved Audit Trail logging
> 1. Exclusive early access to new security features
> 1. Text customisations for your visitors.
> 1. No manual Pro plugin downloads - we handle this all for you automatically.
> 1. No license keys to manage - it's all automatic!
> 1. (coming soon) White Labelling
> 1. (coming soon) Statistics and Reporting
> 1. (coming soon) Select individual automatic plugin updates

### Dedicated Premium Support

The Shield Security team prioritises email technical support over the WordPress.org forums.
Individual, dedicated technical support is only available to customers who have [purchased Shield Pro](https://icwp.io/ab).

Learn more on going Pro at [our One Dollar Plugin store](https://icwp.io/ab).

= Our Mission =

All the great features of how Shield protects your sites and your customers data are set out below in detail, but there are a few things about us, that you should know first:

* We're on a mission to liberate people who manage websites from unnecessarily repetitive work, and by 2022 we want to
be saving our clients over 62.5 million hours per year (and we'd love you to join us in our quest)
* We have three rules that apply to everything we do, and you'll see these when you use our products or contact us for help:

1.  We make everything as simple and easy-to-use as possible (and no simpler!).
1.  We're reliable – we make sure our products do what they promise.
1.  We take ownership for resolving problems - we will solve the problem, or point you towards the solution.

So, read on for the detail, or start protecting yourself, *your clients and your clients' customers* immediately by
downloading and installing Shield now

= What makes the Shield different? =

* Powerful free security protection.
* Easy-To-Setup User Interface.
* It won't break your website - you'll never get that horrible,
	pit-of-your stomach feeling you get with other security plugins when your website doesn't load anymore.
* Super Admin Security - the *only* WordPress Security Plugin that protects against tampering.
* Exclusive membership to a private security group where you can learn more about WordPress security.

= Super Admin Security Protection =
The **only** WordPress security plugin with a WordPress-independent security key to protect itself. [more info](https://icwp.io/wpsf05)

= Audit Trail Activity Monitor =
With the Audit Trail you can review all major actions that have taken place on your WordPress site, by all users.

= Firewall Protection =
Blocks all web requests to the site that violate the firewall security rules! [more info](https://icwp.io/wpsf06)

= Brute Force Login Guard and Two-Factor Authentication =
Provides effective security against Brute Force Hacking and email based Two-Factor Authenticated login. [more info](https://icwp.io/wpsf07)

= Comment SPAM (Full replacement and upgrade from Akismet) =
Blocks **ALL** automatic Bot-SPAM, and catches Human Comments SPAM without sending data to 3rd parties or charging subscription fees. [more info](https://icwp.io/wpsf08)

= FABLE - Fully Automatic Black Listing Engine =
No more manual IP Black lists. This plugin handles the blocking of IP addresses for hosts that are naughty.

= WordPress Lock Down =
Numerous security and protection mechanisms to lock down your WordPress admin area, such as blocking file edits and enforcing SSL.

= Automatic Updates =
Take back control of your WordPress Automatic Updates.

[youtube http://www.youtube.com/watch?v=r307fu3Eqbo]

= Login and Identity Security Protection - Stops Brute Force Attacks =

Note: Login Guard is a completely independent feature to the Firewall.

With the Login Guard this plugin will single-handedly prevent brute force login attacks on all your WordPress sites.

It doesn't need IP Address Ban Lists (which are actually useless anyway), and instead puts hard limits on your WordPress site,
and force users to verify themselves when they login.

Three core security features provide layers to protect the WordPress Login system.

1.	[Email-based 2-Factor Login Authentication](https://icwp.io/2v) based on IP address! (prevents brute force login attacks)
1.	[Login Cooldown Interval](https://icwp.io/2t) - WordPress will only process 1 login per interval in seconds (prevents brute force login attacks)
1.	[GASP Anti-Bot Login Form Protection](https://icwp.io/2u) - Adds 2 protection checks for all WordPress login attempts (prevents brute force login attacks using Bots)

These options alone will protect and secure your WordPress sites from nearly all forms of Brute Force login attacks.

And you hardly need to configure anything! Simply check the options to turn them on, set a cooldown interval and you're instantly protected.

= SPAM and Comments Filtering =

As of version 1.6, this plugin integrates [GASP Spambot Protection](http://wordpress.org/plugins/growmap-anti-spambot-plugin/).

We have taken this functionality a level further and added the concept of unique, per-page visit, Comment Tokens.

**Comment Tokens** are unique keys that are created every time a page loads and they are uniquely generated based on 3 factors:

1.	The visitors IP address.
1.	The Page they are viewing
1.	A unique, random number, generated at the time the page is loaded.

This is all handle automatically and your users will not be affected - they'll still just have a checkbox like the original GASP plugin.

These comment tokens are then embedded in the comment form and must be presented to your WordPress site when a comment is posted.  The plugin
will then examine the token, the IP address from which the comment is coming, and page upon which the comment is being posted.  They must
all match before the comment is accepted.

Furthermore, we place a cooldown (i.e. you must wait X seconds before you can post using that token) and an expiration on these comment tokens.
The reasons for this are:

1.	Cooldown means that a spambot cannot load a page, read the unique comment token and immediately re-post a comment to that page. It must wait
a while.  This has the effect of slowing down the spambots, and, if the spambots get it wrong, they've wasted that token - as tokens can only
be used once.
1.	Expirations mean that a spambot cannot get the token and use it whenever it likes, it must use it within the specfied time.

This all combines to make it much more difficult for spambots (and also human spammers as they have to now wait) to work their dirty magic :)

== Installation ==

Note: When you enable the plugin, the firewall is not automatically turned on. This plugin contains various different sections of
protection for your site and you should choose which you need based on your own requirements.

Why do we do this? It's simple: performance and optimization - there is no reason to automatically turn on features for people that don't
need it as each site and set of requirements is different.

This plugin should install as any other WordPress.org respository plugin.

1.	Browse to Plugins -> Add Plugin
1.	Search: Shield
1.	Click Install
1.	Click to Activate.

A new menu item will appear on the left-hand side called 'Shield'.

== Frequently Asked Questions ==

Please see the dedicated [help centre](https://icwp.io/firewallhelp) for details on features and some FAQs.

= How does the Shield compare with other WordPress Security Plugins? =

Easy - we're just better! ;)

Firstly, we don't modify a single core WordPress or web hosting file. This is important and explains why randomly you upgrade your security plugin and your site dies.

Ideally you shouldn't use this along side other Anti-SPAM plugins or security plugins. If there is a feature you need, please feel free to suggest it in the support forums.

= My server has a firewall, why do I need this plugin? =

This plugin is an application layer firewall, not a server/network firewall.  It is designed to interpret web calls to your site to
look for attempts to circumvent it and gain unauthorized access.

Your network firewall is designed to restrict access to your server based on certain types of network traffic.  The Shield
is designed to restrict access to your site, based on certain type of web calls.

= How does the IP Whitelist work? =

Any IP address that is on the whitelist will not be subject to **any of the firewall processing**.  This setting takes priority over all other settings.

= Does the IP Whitelist support IP ranges? =

Yes. To specify a range you use CIDR notation.  E.g. ABC.DEF.GHJ.KMP/16

= I want to black list an IP address, where can I do that? =

You can't. The plugin runs an automatic black list IP system so you don't need to maintain any manual lists.

= I've locked myself out from my own site! =

This happens when any the following 3 conditions are met:

*	you have added your IP address to the firewall blacklist,
*	you have enabled 2 factor authentication and email doesn't work on your site (and you haven't chosen the override option)

You can completely turn OFF (and ON) the Shield by creating a special file in the plugin folder.

Here's how:

1.	Open up an FTP connection to your site, browse to the plugin folder <your site WordPress root>/wp-content/plugins/wp-simple-firewall/
1.	Create a new file in here called: "forceOff".
1.	Load any page on your WordPress site.
1.	After this, you'll find your Shield has been switched off.

If you want to turn the firewall on in the same way, create a file called "forceOn".

Remember: If you leave one of these files on the server, it will override your on/off settings, so you should delete it when you no longer need it.

= Which takes precedence... whitelist or blacklist? =

Whitelist. So if you have the same address in both lists, it'll be whitelisted and allowed to pass before the blacklist comes into effect.

= What changes go into each version? =

The changelog outlines the main changes for each release. We group changes by minor release "Series". Changes in smaller "point" releases are highlighted
 using **(v.1)** notation.  So for example, version 4.4**.1** will have changelog items appended with **(v.1)**

= Can I assist with development? =

Yes! We actively [develop our plugin on Github](https://github.com/FernleafSystems/wp-simple-firewall) and the best thing you can do is submit pull request and bug reports which we'll review.

= How does the pages/parameters whitelist work? =

It is a comma-separated list of pages and parameters. A NEW LINE should be taken for each new page name and its associated parameters.

The first entry on each line (before the first comma) is the page name. The rest of the items on the line are the parameters.

The following are some simple examples to illustrate:

**edit.php, featured**

On the edit.php page, the parameter with the name 'featured' will be ignored.

**admin.php, url, param01, password**

Any parameters that are passed to the page ending in 'admin.php' with the names 'url', 'param01' and 'password' will
be excluded from the firewall processing.

*, url, param, password

Putting a star first means that these exclusions apply to all pages.  So for every page that is accessed, all the parameters
that are url, param and password will be ignored by the firewall.

= How does the login cooldown work? =

When enabled the plugin will prevent more than 1 login attempt to your site every "so-many" seconds.  So if you enable a login cooldown
of 60 seconds, only 1 login attempt will be processed every 60 seconds.  If you login incorrectly, you wont be able to attempt another
login for a further 60 seconds.

More Info: https://icwp.io/2t

= How does the GASP Login Guard work? =

This is best [described on the blog](https://icwp.io/2u)

= How does the 2-factor authentication work? =

[2-Factor Authentication is best described here](https://icwp.io/2v).

= I'm getting an update message although I have auto update enabled? =

The Automatic (Background) WordPress updates happens on a WordPress schedule - it doesn't happen immediately when an update is detected.
You can either manually upgrade, or WordPress will handle it in due course.

= How can I remove the WordPress admin footer message that displays my IP address? =

You can add some custom code to your functions.php exactly as the following:

`add_filter( 'icwp_wpsf_print_admin_ip_footer', '__return_false' );`

= How can I change the text/html in the Plugin Badge? =

Use the following filter and return the HTML/Text you wish to display:

`add_filter( 'icwp_shield_plugin_badge_text', 'your_function_to_return_text' );`

= How can I change the roles for login notification emails? =

Use the following filter and return the role in the function:

`add_filter( 'icwp_wpsf-login-notification-email-role', 'your_function_to_return_role' );`

Possible options are: network_admin, administrator, editor, author, contributor, subscriber

== Screenshots ==

1. A top-level dashboard that shows all the important things you need to know at-a-glance.
2. IP Whitelist and Blacklists lets you manage access and blocks on your site with ease.
3. A full audit log lets you see everything that happens on your site and why, and by whom.
4. Track user sessions and monitor who is logged-into your site and what they're doing.
5. Simple, clean options pages that let you configure Shield Security and all its options easily.

== Changelog ==

Shield Pro brings exclusive features to the serious webmaster to maximise site security.
You'll also have access to our email technical support team.

You will always be able to use Shield Security and its free features in-full.

[Go Pro for just $1/month](https://icwp.io/aa).

= 8.2.2 - Current Release =
*Released: 14th October, 2019* - [Release Notes](https://icwp.io/g0)

* **(v.2)**  FIXED:		Fixes for scans running under Windows/IIS.
* **(v.2)**  IMPROVED:	Adds a check that a site can send an HTTP request to itself before allowing scans to run.
* **(v.2)**  IMPROVED:	Scans clean up after themselves better, if they fail to run.
* **(v.2)**  IMPROVED:	Server's own IP address detection when site migrated to a new host.
* **(v.2)**  UPDATED:	International translations.
* **(v.2)**  FIXED:		PHP notices when data wasn't as expected.

= 8.2 - Series =
*Released: 1st October, 2019* - [Release Notes](https://icwp.io/g0)

* **(v.1)**  IMPROVED:	Further reduce Malware false positives by also using SVN trunk data when verifying files for plugins and themes.
* **(v.1)**  ADDED:		Initial support for repairing Themes that have been installed from WordPress.org.
* **(v.1)**  ADDED:		Support for using [WP Hashes.com](https://wphashes.com) for WordPress.org themes (already done for plugins).
* **(v.1)**  FIXED:		PHP notices in the logs.
* **(v.0)**  IMPROVED:	[**PRO**] Malware scanner now uses network intelligence to the gather information on malware results.
* **(v.0)**  NEW:		Traffic Watcher feature is now free for all users (no longer Pro-only).
* **(v.0)**  IMPROVED:	Scanning cron is improved and more efficient.
* **(v.0)**  ADDED:		Bulk Delete/Repair/Ignore actions now available for Malware scan results.
* **(v.0)**  IMPROVED:	Malware scan results now provide details of affected line numbers and patterns discovered.
* **(v.0)**  IMPROVED:	Malware scanner only scans `wp-admin`, `wp-includes`, `wp-content` folders, and files in top-level directory.
* **(v.0)**  IMPROVED:	Malware scanner now excludes `wp-content/cache/` directory.
* **(v.0)**  IMPROVED:	Malware scanner performance improved with caching.
* **(v.0)**  IMPROVED:	Malware auto-repair now works more consistently.
* **(v.0)**  IMPROVED:	Updated default firewall whitelist rules.
* **(v.0)**  IMPROVED:	If the PWNED Passwords API request fails entirely, the password check is skipped.
* **(v.0)**  ADDED:		Japanese translations are at 100%.
* **(v.0)**  IMPROVED:	Dutch translations are greatly improved (a huge thank you to Fred!).
* **(v.0)**  FIXED:		Audit Trail correctly logs multiple occurrences for the same type of event on the same page request.
* **(v.0)**  FIXED:		Audit Trail now correctly logs Google reCAPTCHA failure events.
* **(v.0)**  FIXED:		PHP error when firewall was set to kill response without a user message.

= 8.1 - Series =
*Released: 18th September, 2019* - [Release Notes](https://icwp.io/fy)

* **(v.1)**  FIXED:		Error for sites pre-5.0 that don't have function `determine_locale()`
* **(v.0)**  IMPROVED:	Massive improvements to asynchronous scans in performance and reliability.
* **(v.0)**  ADDED:		[**PRO**] Possible to supply multiple email addresses for Administrator login notifications.
* **(v.0)**  ADDED:		New firewall whitelist rule to prevent firewall blocks when activating certain plugins.
* **(v.0)**  IMPROVED:	Prevent errors caused by other plugins not passing correctly-formatted data through WP filters.
* **(v.0)**  ADDED:		Japanese translations (14%).
* **(v.0)**  IMPROVED:	Plugin locale now respects user profile locale setting.
* **(v.0)**  IMPROVED:	Audit Trail filter for specific events.
* **(v.0)**  IMPROVED:	Lots of cleanup of deprecated PHP code following the the v7-v8 upgrade.

= 8.0 - Series =
*Released: 27th August, 2019* - [Release Notes](https://icwp.io/fv)

* **(v.2)**  IMPROVED:	Password strength metering now better aligns with WordPress library (PHP 5.6+)
* **(v.2)**  IMPROVED:	Dutch translations have been adjusted.
* **(v.2)**  FIXED:		Setting 'Month' for IP block duration wasn't being applied.
* **(v.2)**  FIXED:		Certain admin notices not displayed when they should be.
* **(v.1)**  FIXED:		Comment SPAM blocking wasn't working if set to "Detect and Reject".
* **(v.1)**  FIXED:		Shield Widget/Badge broken in some cases.
* **(v.1)**  ADDED:		You can force Shield to operate in any [locale, regardless of site locale](https://icwp.io/gistshieldlocale).
* **(v.1)**  ADDED:		Russian translations are now at 100% and some Dutch translations have been adjusted.
* **(v.0)**  NEW:		[**PRO**] New Malware Scanner with automated file repair for WordPress.org Plugins and Core.
* **(v.0)**  NEW:		Complete overhaul of events system to better audit and collect statistics.
* **(v.0)**  IMPROVED:	Asynchronous scans - scans run in the background and so support more restrictive hosting.
* **(v.0)**  IMPROVED:	Plugin notification system is much improved.
* **(v.0)**  IMPROVED:	[**PRO**] Plugin Guard uses SVN repositories for file references [via WP Hashes API](https://icwp.io/fw).
* **(v.0)**  CHANGED:	Comment SPAM system now uses WordPress Transients API instead of dedicated DB table.
* **(v.0)**  ADDED:		100% Translation coverage for French, Spanish, German, Portuguese, Serbian, Bosnian, Dutch. (Russian on the way)
* **(v.0)**  CHANGED:	Major code cleaning/refactoring for much of the plugin. More to come.

= 7.4 - Series =
*Released: 13th May, 2019* - [Release Notes](https://icwp.io/fc)

* **(v.2)**  NEW:		Options finder/jumper menu lets you find and jump to any option in the plugin instantly.
* **(v.2)**  NEW:		Help/explainer videos for a few sections - more to come.
* **(v.2)**  FIXES:		Fixes for a few problems introduced with the recent UI changes.
* **(v.2)**  FIXED:		Welcome wizard launching was broken.
* **(v.1)**  NEW:		Adjustments and redesign of Shield options pages.
* **(v.1)**  IMPROVED:	Further prep for better internationalization.
* **(v.0)**  NEW:		[**PRO**] [Manual/Automatic User Suspension](https://icwp.io/fa)
* **(v.0)**  NEW:		Comment SPAM - Increase minimum number of approved comments before scanning is skipped
* **(v.0)**  NEW:		[**PRO**] Comment SPAM - Trusted user roles where comments scanning is skipped
* **(v.0)**  IMPROVED:	AntiBot JS was improperly included when not required.
* **(v.0)**  IMPROVED:	Added a GeoIP caching table and removed bundled GeoIP database - greatly reduces download size.
* **(v.0)**  FIXED:		Inconsistent behaviour when PWA plugin is active and it infinitely reloads pages.
* **(v.0)**  FIXED:		Inconsistent behaviour with Anonymous API blocking.
* **(v.0)**  IMPROVED:	Code improvements and refactoring.
* **(v.0)**  ADDED:		Prep for upcoming malware scanner.

#### [Full Changelog](https://icwp.io/shieldwporgfullchangelog)